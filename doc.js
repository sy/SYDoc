function loaddoc (loadtype,loaditem,putto) {
 var to=loaddocurl.replace(/#item#/gi,loaditem);
 to=to.replace(/#type#/gi,loadtype);
 $(putto).html('<img src="loading.gif">加载中');
 $(putto).addClass('docloading');
 $('.docloading').show();
 $.get(to,null,function(data){
  $(putto).html(data);
  $(putto).removeClass('docloading');
 });
}
$('.doctype').click(function(){
 var thistop=$(this).parents('.doclist');
 var thisinfo=$(this).attr('data');
 var nowactive=$(this).parents('.doclist').find('.doctype.active')[0];
 $(thistop).find('ul.active').removeClass('active');
 $(nowactive).addClass('dochover');
 $(nowactive).removeClass('active');
 $(thistop).find('ul[data="'+thisinfo+'"]').addClass('active');
 $(this).addClass('active');
 $(this).removeClass('dochover');
});
$('.docitem').click(function(){
 var thisinfo=$(this).attr('data');
 var thisbody=$(this).parents('.doc').find('.docbody')[0];
 var nowactive=$(this).parents('.doclist').find('.docitem.active')[0];
 if (this==nowactive) return FALSE;
 $(this).removeClass('dochover');
 $(nowactive).addClass('dochover');
 $(nowactive).removeClass('active');
 $(this).addClass('active');
 loaddoc($(this).parent('ul').attr('data'),thisinfo,thisbody);
});